(load "@lib/simul.l")
(load "randomchess.l")
(symbols 'simul 'pico)
(setq *B
   (reverse
      (apply
         mapcar
         (grid 8 8)
         list ) ) )
(de expand (Lst) # Row
   (make
      (for L Lst
         (if (format L)
            (do @
               (link 1) )
            (link L) ) ) ) )
(de dsp ()
   (let N 9
      (prinl "  --------")
      (for Lst *B
         (prin (dec 'N) "|")
         (for This Lst
            (prin (if (=1 (setq @ (: p))) "." @)) )
         (prinl "|") )
      (prinl "  --------")
      (prinl "  abcdefgh") ) )
(de setfen (Str) # eight rows, chopped
   (mapc
      '((A B)
         (mapc
            '((X Y)
               (with X (=: p Y)) )
            A
            B ) )
      *B
      (mapcar expand (split Str "/")) ) )
(de getfen ()
   (glue
      "/"
      (make
         (for Lst *B
            (let C 0
               (link
                  (pack
                     (make
                        (for This Lst
                           (if (=1 (: p))
                              (inc 'C)
                              (when (gt0 C) (link C) (zero C))
                              (link (: p)) ) )
                        (when (gt0 C) (link C) (zero C)) ) ) ) ) ) ) ) )
(de makemove (Str) # "e2e4", "a7b8Q"
   (let (Str (chop Str)
         Sour (pack (head 2 Str))
         Dest (pack (tail 2 (head 4 Str)))
         Piece (get (intern Sour) 'p)
         Promo )
      (when (= (length Str) 5)
         (set 'Promo (last Str)) )
      (put (intern Dest) 'p (if Promo @ Piece))
      (put (intern Sour) 'p 1) ) )

# go movetime DONT print to stdout, cant handle
# jul2021
# (pipe
   # (out '(./fire)
      # (prinl "uci")
      # (prinl "position fen \"r2r2k1/2q1bpp1/3p1n1p/1ppN4/1P1BP3/P5Q1/4RPPP/R5K1 b - - 1 20\" moves f6d5")
      # (prinl "go movetime 3000")
   # )
   # (until (eof)
      # (println 'line (line T))
   # ) )

(msg 'ok)
(bye)
